import "./App.css";
import React, { useEffect } from "react";
import { useState } from "react";
import Carousel from "react-bootstrap/Carousel";
function App() {
  const [imgpath1, setimgpath1] = useState([]);
  const [imgpath2, setimgpath2] = useState([]);
  useEffect(() => {
    fetchDetails();
  }, []);

  const fetchDetails = () => {
    fetch(`http://localhost:8084/image1`).then((response) => {
      setimgpath1(response.url);
    });
    fetch(`http://localhost:8084/image2`).then((response) => {
      setimgpath2(response.url);
    });
  };

  return (
    <div className="App">
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={imgpath1}
            style={{ height: "500px" }}
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={imgpath2}
            style={{ height: "500px" }}
            alt="Second slide"
          />
          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

export default App;
